import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  FlatList
} from "react-native";
import Card from "./src/shared/components/card/card";
import { movieService } from "./src/services/movie-services";

export default class App extends React.Component {
  state = {
    movies: [],
    loading: true
  };
  componentDidMount() {
    movieService
      .getPopularMovies()
      .then(movies => {
        this.setState({
          movies: movies,
          loading: false
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  getMovieTemplate(title, description, imageUrl) {
    return <Card 
                  title={title} 
                  description={description} 
                  imageUrl={imageUrl} />;
  }

  render() {
    return (
      <View style={styles.container}>
        {!this.state.loading ? (
          <FlatList
            data={this.state.movies}
            renderItem={({ item }) =>
              this.getMovieTemplate(
                item.title, 
                item.description,
                item.poster_path)
            }
          />
        ) : null}

        {this.state.loading ? <Text>Carregando...</Text> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1
  }
});
