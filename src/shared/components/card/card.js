import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

export default class Card extends React.Component {
  render() {
    const title = this.props.title;
    const description = this.props.description;

    return (
      <View style={[
          styles.container, 
          this.props.style || null
        ]}>
        <View>
            <Text
                style={{
                    textAlign: "center",
                    fontSize: 24
                }}
            >{ title }</Text>
        </View>
        
        <View>
            <Image
            style={styles.imageStyle}
            source={
              { uri: "https://image.tmdb.org/t/p/w500" + this.props.imageUrl}
            }
            />
        </View>

        <View>
            <Text 
                style={{
                    padding: 12, 
                    paddingTop: 5, 
                    textAlign: "center"}
                }>{ description }</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    padding: 16
  },

  imageStyle: {
    height: 150,
    width: 340
  }
});
