import axios from "axios";
const baseurl = "https://api.themoviedb.org/3/";
const moviebaseurl = baseurl + "movie/";
const api_key = "api_key=8136ba8628449279b2c63880b5e3ea7c";

export const movieService = {
    getPopularMovies: () => {
        const url = `${moviebaseurl}popular?${api_key}`;
        return new Promise((resolve, reject) => {
            var movies = [];
            axios
                .get(url)
                .then(response => {
                    movies = response.data.results;
                    resolve(movies);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
}